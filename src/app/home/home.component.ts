import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  Task: any = [{ }];

  form: any = {
      id:0,
      description:'',
      date:'',
      isDone:''
  }

  ordering: any = {
      id:false,
      description: false,
      date: false,
      isDone: false
  }

  constructor() {}

  ngOnInit() {
      this.loadTasks();
  }

  setForm = (ev: any): void => {
      this.form[ev.target.name] = ev.target.value;
  }

  atualizaForm = (form: any): void => {
      this.form = form;
  }


  insertTask = (description: String, date: String): void => {

      fetch("http://localhost:8000/training/v1/tasks", {
          body: JSON.stringify({date:date,description:description}),
          headers: {
              "Content-Type": "application/json",
          },
          method: "POST",
      }).then(r => r.text()).then(() => {
               this.loadTasks();
      });
  }

  loadTasks = (): void => {

      fetch("http://localhost:8000/training/v1/tasks", {
          method: "GET",
      }).then(text => text.text()).then(response => {
          this.Task = JSON.parse(response).taskJsonResponse; 
      });    
  }

  deleteTask = (id_task: Number): void => {

      if (confirm("tem certeza que deja excluir a task "+id_task+" ?")) {

          fetch("http://localhost:8000/training/v1/tasks/delete", {
              body: JSON.stringify({id_task:id_task}),
              headers: {
                  "Content-Type": "application/json",
              },
              method: "POST",
          }).then(() => this.loadTasks())

      }
  }

  alterTask = (id: Number, description: String, date: String, isDone: boolean): void => {

      fetch("http://localhost:8000/training/v1/tasks/alter", {
          body: JSON.stringify({id:id,description:description,date:date,isDone:isDone}),
          headers: {
              "Content-Type": "application/json",
          },
          method: "POST",
      }).then(() => this.loadTasks())
  }

  orderingColumns = (column: String): void => {
      switch (column) {

          case 'id':

              this.ordering.id = !this.ordering.id;

              if (this.ordering.id) {
                  this.Task.sort((a,b) => (a.id.toLowerCase() > b.id.toLowerCase()) ? 1 : ((b.id.toLowerCase() > a.id.toLowerCase()) ? -1 : 0)); 
              } else {
                  this.Task.sort((a,b) => (a.id.toLowerCase() > b.id.toLowerCase()) ? 1 : ((b.id.toLowerCase() > a.description.toLowerCase()) ? -1 : 0)).reverse(); 
              }

          break;

          case 'description':

              this.ordering.description = !this.ordering.description;

              if (this.ordering.description) {
                  this.Task.sort((a,b) => (a.description.toLowerCase() > b.description.toLowerCase()) ? 1 : ((b.description.toLowerCase() > a.description.toLowerCase()) ? -1 : 0)); 
              } else {
                  this.Task.sort((a,b) => (a.description.toLowerCase() > b.description.toLowerCase()) ? 1 : ((b.description.toLowerCase() > a.description.toLowerCase()) ? -1 : 0)).reverse(); 
              }

          break;

          case 'date':

              this.ordering.date = !this.ordering.date;

              if (this.ordering.date) {
                  this.Task.sort((a,b) => (a.date.toLowerCase() > b.date.toLowerCase()) ? 1 : ((b.date.toLowerCase() > a.date.toLowerCase()) ? -1 : 0)); 
              } else {
                  this.Task.sort((a,b) => (a.date.toLowerCase() > b.date.toLowerCase()) ? 1 : ((b.date.toLowerCase() > a.date.toLowerCase()) ? -1 : 0)).reverse(); 
              }

          break;

          case 'isDone':

              this.ordering.isDone = !this.ordering.isDone;

              if (this.ordering.isDone) {
                  this.Task.sort((a,b) => (a.isDone > b.isDone) ? 1 : ((b.isDone > a.isDone) ? -1 : 0)); 
              } else {
                  this.Task.sort((a,b) => (a.isDone > b.isDone) ? 1 : ((b.isDone > a.isDone) ? -1 : 0)).reverse(); 
              }

          break;
      }
  }


}
